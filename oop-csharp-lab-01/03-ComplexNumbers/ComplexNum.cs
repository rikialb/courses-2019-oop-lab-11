﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public double Re { get; private set; }
        public double Im { get; private set; }

        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Re, 2) + Math.Pow(this.Im, 2));
                //throw new NotImplementedException();
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, -this.Im);
                //throw new NotImplementedException();
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return ("Parte Reale " + this.Re + "Parte Immaginaria " + this.Im);
            //throw new NotImplementedException();
        }
    }
}
